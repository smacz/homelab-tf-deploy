locals {
  env_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  # I tried to include a file that was in the same directory as the terragrunt.hcl,
  # but it didn't work:
  #
  # instance_vars = read_terragrunt_config("instance.hcl")
  #
  # and
  #
  # instance_vars = read_terragrunt_config("${get_terragrunt_dir()}/instance.hcl")
  #
  # So we'll just do this for now
  instance_vars = read_terragrunt_config(find_in_parent_folders("manager.hcl"))
  source_base_url = "gitlab.com/smacz/homelab-tf-modules.git//haproxy"
}

inputs = {
  node_name = "proxform"
  app_name = "manager"
  deploy_version_number = "${local.instance_vars.locals.deploy_version_number}"
  template_file_id = "${local.env_vars.locals.template_file_id}"
  nixos_app_config = <<-EOT
services.consul = {
  enable = true;
  webUi = true;
  extraConfig = {
    bootstrap = true;
    server = true;
    client_addr = "0.0.0.0";
  };
};
EOT
}
