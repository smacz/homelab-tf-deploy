locals {
  env_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  global_vars = read_terragrunt_config(find_in_parent_folders())
  app_name = "consul"
  app_firewall_rules = [
    {
      type = "out"
      action = "ACCEPT"
      comment = "Allow Inter-Servers Consul Traffic"
      dest = "+dc/consul_${local.env_vars.locals.env_name}_ipset"
      dport = "8300"
      proto = "tcp"
      log = "info"
    }
  ]
  nixos_service_config = <<-EOT
{ config, pkgs, ... }:

{
  services.consul = {
    enable = true;
    webUi = true;
    extraConfig = {
      bootstrap = true;
      server = true;
      client_addr = "0.0.0.0";
    };
  };
}
EOT
}

inputs = {
  app_name = "${local.app_name}"
  app_firewall_rules = "${local.app_firewall_rules}"
  nixos_service_config = "${local.nixos_service_config}"
}
