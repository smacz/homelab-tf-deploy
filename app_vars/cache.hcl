locals {
  env_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  global_vars = read_terragrunt_config(find_in_parent_folders())
  app_name = "cache"
  app_firewall_rules = [
    {
      type = "out"
      action = "ACCEPT"
      comment = "Allow access to cache.nixos.org"
      # This is current the endpoint for cache.nixos.org
      # which is an alias for dualstack.v2.shared.global.fastly.net
      dest = "146.75.78.217"
      dport = "80"
      proto = "tcp"
      log = "info"
    },
    {
      type = "out"
      action = "ACCEPT"
      comment = "Allow access to cache.nixos.org"
      # This is current the endpoint for cache.nixos.org
      # which is an alias for dualstack.v2.shared.global.fastly.net
      dest = "146.75.78.217"
      dport = "443"
      proto = "tcp"
      log = "info"
    }
  ]
  # From https://dataswamp.org/~solene/2022-06-02-nixos-local-cache.html
  nixos_service_config = <<-EOT
{ config, pkgs, ... }:

{
  services.nginx = {
    enable = true;
    appendHttpConfig = ''
      proxy_cache_path /var/cache/nginx levels=1:2 keys_zone=cachecache:100m max_size=20g inactive=365d use_temp_path=off;

      # Cache only success status codes; in particular we don't want to cache 404s.
      # See https://serverfault.com/a/690258/128321
      map $status $cache_header {
        200     "public";
        302     "public";
        default "no-cache";
      }
      access_log /var/log/nginx/access.log;
    '';

    virtualHosts."nixcache.${local.global_vars.locals.domain}" = {
      locations."/" = {
        root = "/var/public-nix-cache";
        extraConfig = ''
          expires max;
          add_header Cache-Control $cache_header always;
          # Ask the upstream server if a file isn't available locally
          error_page 404 = @fallback;
        '';
      };

      extraConfig = ''
        # Using a variable for the upstream endpoint to ensure that it is
        # resolved at runtime as opposed to once when the config file is loaded
        # and then cached forever (we don't want that):
        # see https://tenzer.dk/nginx-with-dynamic-upstreams/
        # This fixes errors like
        #   nginx: [emerg] host not found in upstream "upstream.example.com"
        # when the upstream host is not reachable for a short time when
        # nginx is started.
        resolver ${local.env_vars.locals.dns_server};
        set $upstream_endpoint http://cache.nixos.org;
      '';

      locations."@fallback" = {
        proxyPass = "$upstream_endpoint";
        extraConfig = ''
          proxy_cache cachecache;
          proxy_cache_valid  200 302  60d;
          expires max;
          add_header Cache-Control $cache_header always;
        '';
      };

      # We always want to copy cache.nixos.org's nix-cache-info file,
      # and ignore our own, because `nix-push` by default generates one
      # without `Priority` field, and thus that file by default has priority
      # 50 (compared to cache.nixos.org's `Priority: 40`), which will make
      # download clients prefer `cache.nixos.org` over our binary cache.
      locations."= /nix-cache-info" = {
        # Note: This is duplicated with the `@fallback` above,
        # would be nicer if we could redirect to the @fallback instead.
        proxyPass = "$upstream_endpoint";
        extraConfig = ''
          proxy_cache cachecache;
          proxy_cache_valid  200 302  60d;
          expires max;
          add_header Cache-Control $cache_header always;
        '';
      };
    };
  };
}
EOT
}

inputs = {
  app_name = "${local.app_name}"
  app_firewall_rules = "${local.app_firewall_rules}"
  nixos_service_config = "${local.nixos_service_config}"
}
