locals {
  env_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  # I tried to include a file that was in the same directory as the terragrunt.hcl,
  # but it didn't work:
  #
  # instance_vars = read_terragrunt_config("instance.hcl")
  #
  # and
  #
  # instance_vars = read_terragrunt_config("${get_terragrunt_dir()}/instance.hcl")
  #
  # So we'll just do this for now
  instance_vars = read_terragrunt_config(find_in_parent_folders("haproxy.hcl"))
  source_base_url = "gitlab.com/smacz/homelab-tf-modules.git//haproxy"
}

inputs = {
  app_name = "haproxy"
# We can do stuff like the following too:
#
# mysql = "${local.env_vars.locals.mysql}"
# springboot_secrets = [
#   {
#     var_file_key = "spring_datasource_password"
#     vault_path = "cartographer/database"
#     vault_key = "password"
#   }
# ]
}
