include "root" {
  path = find_in_parent_folders()
}

include "app_vars" {
  path = "${get_terragrunt_dir()}/../../app_vars/manager.hcl"
}

# Construct the terraform.source attribute using the source_base_url and customer version
terraform {
  # source = "${include.app_vars.locals.source_base_url}?ref=0.0.1"
  source = "git::ssh://git@gitlab.com/smacz/homelab-tf-modules.git//nixos-proxmox-container"
}
