dependency "cache" {
  config_path = "./cache"
}

dependency "consul" {
  config_path = "./consul"
}

dependency "haproxy" {
  config_path = "./haproxy"
}

inputs = {
  cache_complete = dependency.cache.outputs.complete
  consul_complete = dependency.cache.consul.complete
  haproxy_complete = dependency.cache.consul.complete
}

locals {
  haproxy_provisioned = dependency.haproxy.state == "available"
}

resource "consul" "service" {
  // Ensure Consul depends on HAProxy
  depends_on = [dependency.haproxy]

  triggers = {
    haproxy_state = dependency.haproxy.state
  }

  ip_address = var.haproxy_ip
}
