include "root" {
  path = find_in_parent_folders()
}

include "app_vars" {
  path = "${dirname(find_in_parent_folders())}/app_vars/cache.hcl"
}

include "env_vars" {
  path = "${find_in_parent_folders("env.hcl")}"
}

terraform {
  # source = "${include.app_vars.locals.source_base_url}?ref=0.0.1"
  #source = "git::ssh://git@gitlab.com/smacz/homelab-tf-modules.git//nixos-proxmox-container"
  source = "../../../homelab-tf-modules/nixos-proxmox-container"
}

inputs = {
    container_count = 1
    deploy_version_number = "1"
    cpu_cores = 2
    memory_dedicated = 2048
}
