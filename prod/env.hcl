locals {
  vlan_id = 5
  bridge = "vmbr5"
  dns_server = "172.16.5.1"
  nixos_version = "23.11"
  env_name = "prod"
  # Other vars can go here, like...
  #
  # haproxy = {
  #   vip = "ind-test-haproxy.updoxnet.com"
  #   backends = [
  #     "ind-test-haproxy01.hub.andrewcz.com"
  #     "ind-test-haproxy01.hub.andrewcz.com"
  #   ]
  # }
  env_firewall_rules = [
    {
      type = "out"
      action = "ACCEPT"
      comment = "Allow DNS communication"
      dest = "172.16.5.1"
      dport = "53"
      proto = "udp"
      log = "info"
    },
    {
      type = "out"
      action = "ACCEPT"
      comment = "Allow DHCP communication"
      dest = "172.16.5.1"
      dport = "67:68"
      proto = "tcp"
      log = "info"
    },
    {
      type = "out"
      action = "ACCEPT"
      comment = "Allow LDAP communication"
      dest = "+ldap_prod_ipset"
      dport = "636"
      proto = "tcp"
      log = "info"
    },
  ]
  template_file_id = "local:vztmpl/nixos-system-x86_64-linux.${local.nixos_version}.homelab.tar.gz"
}

inputs = {
  vlan_id = "${local.vlan_id}"
  bridge = "${local.bridge}"
  dns_server = "${local.dns_server}"
  env_name = "${local.env_name}"
  env_firewall_rules = "${local.env_firewall_rules}"
  nixos_version = "${local.nixos_version}"
  template_file_id = "${local.template_file_id}"
}
