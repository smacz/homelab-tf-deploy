locals {
  # These are the global vars, but in reality, these could be the location vars underneath a
  # consuming global structure. However, since I only have the one location, this is the top-level
  # under which all CTs will be created
  proxmox_vars = jsondecode(read_tfvars_file("${get_parent_terragrunt_dir()}/proxmox.tfvars"))
  proxmox_secrets = jsondecode(sops_decrypt_file("${get_parent_terragrunt_dir()}/proxmox_secrets.json"))
  proxmox_node_name = "proxform"
  domain = "hub.andrewcz.com"
}

inputs = {
  domain = "${local.domain}"
  proxmox_user = "${local.proxmox_vars.proxmox_user}"
  proxmox_pass = "${local.proxmox_secrets.proxmox_pass}"
  proxmox_url = "${local.proxmox_vars.proxmox_url}"
  proxmox_node_name = "${local.proxmox_node_name}"
  proxmox_host = "${local.proxmox_node_name}.${local.domain}"
}

# Generate providers.tf file with proxmox, random, and ssh
generate "provider" {
  path = "providers.tf"
  if_exists = "overwrite_terragrunt"
  contents = <<EOF
terraform {
  required_providers {
    proxmox = {
      #source = "local/local/proxmox"
      source = "bpg/proxmox"
    }
    random = {
    }
    ssh = {
      source = "loafoe/ssh"
    }
  }
}
provider "proxmox" {
  endpoint = "https://${local.proxmox_node_name}.${local.domain}:8006/"
  insecure = true
  username = "${local.proxmox_vars.proxmox_user}@${local.proxmox_vars.proxmox_user_realm}"
  password = "${local.proxmox_secrets.proxmox_pass}"
  ssh {
    username = "${local.proxmox_vars.proxmox_user}"
    password = "${local.proxmox_secrets.proxmox_pass}"
  }
}
EOF
}

remote_state {
  backend = "local"
  config = {
    path = "${get_parent_terragrunt_dir()}/${path_relative_to_include()}/terraform.tfstate"
  }

  generate = {
    path = "backend.tf"
    if_exists = "overwrite"
  }
}
